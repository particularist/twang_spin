use fon::{stereo::Stereo32, Sink};
use twang::{Mix, Synth, Fc, Signal};
use wavy::{Speakers, SpeakersSink};
use pasts::{exec, wait};

 // Target sample rate set to 48 KHz
 const S_RATE: u32 = 48_000;

/// First ten harmonic volumes of a piano sample (sounds like electric piano).
const HARMONICS: [f64; 10] = [
    0.700, 0.243, 0.229, 0.095, 0.139, 0.087, 0.288, 0.199, 0.124, 0.090,
    ];
/// The three pitches in a perfectly tuned A3 minor chord
const PITCHES: [f64; 3] = [220.0, 220.0 * 32.0 / 27.0, 220.0 * 3.0 / 2.0];
/// Volume of the piano
const VOLUME: f64 = 0.1;

/// An event handled by the event loop.
enum Event<'a> {
    /// Speaker is ready to play more audio.
    Play(SpeakersSink<'a, Stereo32>),
}

struct State {
    /// A streaming synthesizer using Twang.
    synth: Synth<()>,
    duration: usize
}

impl State {

    /// Event loop.  Return false to stop program.
    fn event(&mut self, event: Event<'_>) {
        match event {
            Event::Play(mut speakers) => {
                if self.duration == 0 {
                    std::process::exit(0);
                }else{
                    self.duration -= 1;
                    speakers.stream(&mut self.synth)
                }
            },
        }
    }
}

fn main() {
   fn piano(_: &mut (), fc: Fc) -> Signal {
       PITCHES
           .iter()
           .map(|p| {
               HARMONICS
                   .iter()
                   .enumerate()
                   .map(|(i, v)| {
                       fc.freq(p * (i + 1) as f64).sine().gain(v * VOLUME)
                   })
                   .mix()
           })
           .mix()
  }

  let mut speakers = Speakers::default();
  // Create the synthesizer.
  let mut state = State {
      duration: 200,
      synth: Synth::new((), piano),
  };

  exec!(state.event(wait! {
      Event::Play(speakers.play().await),
  }));
}
